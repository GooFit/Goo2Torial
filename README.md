# GooFit 2torial

Welcome to the new tutorial series for GooFit. This series is intended to replace [GooTorial] as the go-to resource for learning to use GooFit, designed for GooFit 2.0. This series is intended to be maintainable as GooFit evolves.

This book is divided into several sections. The first covers GPU computing from a general perspective, slowly approaching the design of GooFit. It ends up building a home-grown example of a GooFit-like program.

The second section focus on [GooFit].

This book is under construction. Clicking on a page that has not been written yet will not do anything.

Also see the [Hydra] project for anther more general approach to high-performance fitting.

[GooTorial]: https://github.com/GooFit/GooTorial
[GooFit]: https://github.com/GooFit/GooFit
[Hydra]: https://github.com/MultithreadCorner/Hydra

