# Adding a PDF

The following outlines the process to add a PDF. The PDF example simply multiplies a value by a number that can't change.

## Create: `include/goofit/PDFs/folder/NamePdf.h`

```cpp
#pragma once

#include <goofit/PDFs/GooPdf.h>

namespace GooFit {

class NamePdf : public GooPdf {
public:
    NamePdf (std::string n, Variable* x, fptype v);
};

} // GooFit
```

## Create: `src/PDFs/folder/NamePdf.cu`

```cpp
#include <goofit/PDFs/utility/NamePdf.h>

using namespace std;

namespace GooFit {

__device__ fptype device_NamePdf (fptype* evt, fptype* p, unsigned int* indices) {
  int ret = 0;
  fptype obsValue = evt[indices[2 + indices[0]]];
  int numConstants = indices[1];
  fptype number = functorConstants[indices[0+2]];

  return obsValue * number;
}

__device__ device_function_ptr ptr_to_NamePdf = device_NamePdf;

__host__ NamePdf::NamePdf (std::string n, Variable* x, fptype v)
  : GooPdf(x, n) {
  
  // Storing one constant
  size_t numConstants = 1;
  cIndex = registerConstants(numConstants);
  std::vector<fptype> host_constants {v};
  
  std::vector<size_t> pindices {numConstants};


  MEMCPY_TO_SYMBOL(
      functorConstants,
      host_constants,
      numConstants*sizeof(fptype),
      cIndex*sizeof(fptype),
      cudaMemcpyHostToDevice);

  GET_FUNCTION_ADDR(ptr_to_NamePdf);
  initialise(pindices);
}

} // GooFit
```

## Modify: `src/PDFs/CMakeLists.txt`

Add your header and source files to the respective lines.



## Create: `python/PDFs/folder/NamePdf.cu`

```cpp
#include <pybind11/pybind11.h>

#include <goofit/Variable.h>
#include <goofit/PDFs/utility/NamePdf.h>

using namespace GooFit;
namespace py = pybind11;

void init_NamePdf(py::module &m) {
    py::class_<NamePdf, GooPdf>(m, "NamePdf")
        .def(py::init<std::string, Variable *, fptype>())
        ;
}
```
 
## Modify: `python/CMakeLists.txt`

Add a line for your python `.cu` file.

## Modify: `python/goofit.cpp`

Add a define for your init function:

```cpp
 void init_NamePdf(py::module &);
```

And add the function in the list of init functions:

```cpp
    init_NamePdf(m);
```
