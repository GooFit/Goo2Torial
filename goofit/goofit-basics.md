# A Basic Fit With GooFit

In this example we are going to look at fitting a Gaussian signal with a polynomial background just as you might be familiar with in RooFit. This example can be found in the build directory under Addition. What we are looking at will be similar to `examples/addition/addition.cpp` or `python/examples/addition.py`.

First let us look at the top of most GooFit programs, starting with the imports:

{% codegroup %}
```cpp
#include <goofit/Application.h>     // The Application
#include <goofit/Variable.h>        // Observables, variables, etc. 
#include <goofit/utilities/Style.h> // Optional styling
#include <TH1D.h>                   // ROOT includes
```

```python
import goofit as gf
import numpy as np
```
{% endcodegroup %}

Followed by the first few lines of code:


{% codegroup %}
```cpp
int main(int arg, char** argv) {

    GooFit::Application app("Addition example", argc, argv);

    // Custom commandline options would go here
    GOOFIT_PARSE(app);

    // Nice ROOT plot style defaults (optional)
    GooFit::setROOTStyle();
```

```python
# Print out GooFit information
print(gf.goofit_info())
```
{% endcodegroup %}


Now you may notice the obvious lines mentioning Application and PARSE. These allow GooFit to offer a way to provide extra functionality such as choosing what GPU device to run GooFit on. It also sets up some global utilities for GooFit that are needed for some features, like MPI support. It also provides powerful tools for building a command line interface; see [CLI11](https://github.com/CLIUtils/CLI11).

We also use a mildly updated style for ROOT; you can do your own styling instead.

Now we setup the variables which we will need.

{% codegroup %}
```cpp
// Our variable which we are fitting
GooFit::Observable xvar{"xvar", -5, 5};
    
// The dataset
GooFit::UnbinnedDataSet data{xvar};
    
// An ordinary ROOT histogram
TH1F xvarHist{"xvarHist", "",
              xvar.getNumBins(),
              xvar.getLowerLimit(),
              xvar.getUpperLimit()};
xvarHist.SetStats(false);
```

```python
# Our variable which we are fitting
xvar = gf.Observable("xvar", -5, 5)
    
# The dataset
data = gf.UnbinnedDataSet(xvar)
```

{% endcodegroup %}


Ignoring all the familiar ROOT things, we come to something which may seem familiar from RooFit: an Observable, which is similar to `RooRealVar` and simply represents the variable which we are fitting. We also have `UnbinnedDataSet` which is `GooFit` class for storing unbinned data. In this case we are making a dataset that will hold `xvar` data in an instance called `data`.

OK, now let's get some toy data. In this example we are going to produce a Gaussian signal with a uniform background and add it to our data using the `addEvent` function. There are several ways to fill this; we will show ROOT, since it is available. We could use C++11's random generators, which are better but more limited in available distributions.


{% codegroup %}
```cpp
TRandom3 donram{42};

// Fill while the dataset has less than 100,000 entries
while(data.getNumEvents() < 100000) {
    
	// Generate a mix of the two distributions
	// We could use .setValue() instead, but this is cleaner
    if(donram.Uniform() < 0.1)
        xvar = donram.Uniform(xvar.getLowerLimit(),
                              xvar.getUpperLimit());
    else
        xvar = donram.Gaus(0.2, 1.1);

    // Skip to the next random number if outside range
    // A variable is "false" if outside the set range
    if(xvar) {
    
        // Current values are captured when filling a DataSet
        data.addEvent();
        
        // Fill the histogram, too
        xvarHist.Fill(xvar.getValue());   
    }
}
```

```python
# Generate from two distributions
# They will be sorted by distribution but that's okay
dat = np.random.normal(0.2,1.1,100000)
data[:10000] = np.random.uniform(-5,5,10000)

# This will have slighly less than 100,000
# events due to filtering out of range
data.from_matrix([dat], filter=True)
```

{% endcodegroup %}

### Fit parameters and PDFs

Now we have our data, let's build our PDFs. Once again we have some objects which might be familiar from RooFit.


{% codegroup %}
```cpp
// Variables have: name, value, error, lower, upper
Variable xmean{"xmean", 0, 1, -10, 10};
Variable xsigm{"xsigm", 1, 0.5, 1.5};
// PDFs take Variables by value (safe to delete and copy)
GaussianPdf signal{"signal", xvar, xmean, xsigm};

Variable constant{"constant", 1.0};
PolynomialPdf backgr{"backgr", xvar, {constant}};
    
// The weighting of our PDF components
Variable sigFrac{"sigFrac", 0.9, 0.75, 1.00};

// PDFs take other PDFs by pointer
// (do not delete the original PDFs)
AddPdf total{"total", {sigFrac}, {&signal, &backgr}};
```

```python
# Variables have: name, value, error, lower, upper
xmean = Variable("xmean", 0, 1, -10, 10)
xsigm = Variable("xsigm", 1, 0.5, 1.5)
signal = GaussianPdf("signal", xvar, xmean, xsigm)

constant = Variable("constant", 1.0)
backgr = PolynomialPdf("backgr", xvar, [constant])

# The weighting of our PDF components
sigfrac = Variable("sigFrac", 0.9, 0.75, 1.00)

total = AddPdf("total", [sigfrac], [signal, backgr])
```
{% endcodegroup %}

Our fitting parameters are defined using the `Variable` class. looking at the sigma of our soon to be Gaussian Function, `xsigm` , we first define a string with the name of the variable, then it's initial starting value and the range it is constrained to . There is an additional option a variable may have and that is an error scale which is an additional parameter as seen with `xmean` . The constructor can be of the form:

* `Variable("name", starting value)`: Constant
* `Variable("name", starting value, error scale)`: Free
* `Variable("name", starting value, error scale, minimum, maximum)`: Limited

Once we have our background and signal parameterization we are ready to build our model. We do this by using the `AddPdf` function which takes in our PDF weights and functions, note the syntax - you need to give it a list or vector of PDF pointers. The final step to performing our fit is the following.

{% codegroup %}
```cpp
total->fitTo(data);
```

```python
total.fitTo(data)
```
{% endcodegroup %}



Finally, we can plot if we can also evaluate the PDFs points over a grid. Observables have an inherent binning number (which you can set with `setNumBins(N)`), and we can evaluate over than binning:



{% codegroup %}
```cpp
UnbinnedDataSet grid = total.makeGrid();
total.setData(grid);

// Evaluate PDF and components at set data values
std::vector<std::vector<fptype>> values
    = total.getCompProbsAtDataPoints();
```

```python
grid = total.makeGrid()
total.setData(grid)
main, gauss, flat = total.getCompProbsAtDataPoints()
xvals = grid.to_matrix().flatten()
```
{% endcodegroup %}

Now, grid will contain the points, and values will have three arrays in it; the first is the total PDF, and the other two are the two component PDFs.

To use this, you can do something like this. Starting with general ROOT setup:

{% codegroup %}
```cpp
TH1F pdfHist("pdfHist", "", xvar.getNumBins(), xvar.getLowerLimit(), xvar.getUpperLimit());
TH1F sigHist("sigHist", "", xvar.getNumBins(), xvar.getLowerLimit(), xvar.getUpperLimit());
TH1F bkgHist("bkgHist", "", xvar.getNumBins(), xvar.getLowerLimit(), xvar.getUpperLimit());

TCanvas foo;

// Now, we can loop over the points in the grid and load the ROOT histograms:
for(int i=0; i<grid.getNumEvents(); ++i) {
    // Fills the linked Observables with the current values
    grid.loadEvent(i);
    
    // Input to the ROOT histograms
    pdfHist.Fill(xvar.getValue(), pdfVals[0][i]);
    sigHist.Fill(xvar.getValue(), pdfVals[1][i]);
    bkgHist.Fill(xvar.getValue(), pdfVals[2][i]);
}

xvarHist.Draw("p");
pdfHist.Draw("lsame");
sigHist.Draw("lsame");
bkgHist.Draw("lsame");
```

```python
plt.plot(xvals, main, label='total')
plt.plot(xvals, np.array(gauss)*sigfrac.value, label='signal')
plt.plot(xvals, np.array(flat)*(1-sigfrac.value), label='background')

plt.legend()
plt.show()
```

{% endcodegroup %}



> For the truly lazy, you can also do the following to make a TH1D with function values of the master PDF in it if you have ROOT support:
> 
> ```cpp
TH1D* funct_hist = total.plotToROOT(xvar);
```

