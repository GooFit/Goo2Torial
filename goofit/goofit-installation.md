# GooFit installation

## Getting the files

Clone with git:

{% term %}
gitbook:~ $ git clone git://github.com/GooFit/GooFit.git --recursive --branch=v2.0.0
gitbook:~ $ cd GooFit
{% endterm %}

The `--recursive` option will download all submodules and is optional; CMake will use git to download the submodules for you if you forget. The `--branch` command checks out a tag (release) or a branch; there is a `stable` branch that points to the latest release plus recent bugfixes and a `master` branch that points to the latest accepted changes toward the next version.

## Building 

The build system uses CMake. The procedure is standard for CMake builds:

{% term %}
gitbook:GooFit $ mkdir build
gitbook:GooFit $ cd build
gitbook:build $ cmake ..
gitbook:build $ make
{% endterm %}

If you don't have a modern CMake, Kitware provides installers for every OS. You can even get a copy using python: `pip install cmake` or locally with `pip install --user cmake`. On a Mac, you can also use any package manager, such as Homebrew: `brew install cmake`

If you want to change compiler, set `CC` and `CXX` to appropriate defaults *before* you run CMake either inline or in your environment. You can also set `CMAKE_C_COMPILER` and `CMAKE_CXX_COMPILER` directly on the command line with `-D`. If you want to set the host and device backends, you can set those options.

Valid options are `CUDA` (device only), `OMP`, `TBB`, and `CPP`. The Thrust `TBB` backend requires the Intel compiler.  The default device is `Auto`, and will select `CUDA` if CUDA is found, `OMP` or `CPP` otherwise.

Other custom options supported along with the defaults:

* `-DGOOFIT_DEVICE=Auto`: The device to use for computation (`CUDA`, `OMP`, `TBB`, or `CPP`). Default setting of `Auto` looks for CUDA first, then OpenMP, then CPP.
* `-DGOOFIT_ARCH=Auto`: (`Auto`, `Common`, `All`, valid number(s) or name(s)): sets the compute architecture. See [CUDA_SELECT_NVCC_ARCH_FLAGS].
* `-DGOOFIT_EXAMPLES=ON`: Build the examples
* `-DGOOFIT_PACKAGES=ON`: Build any packages found with the name `goofit*`
* `-DGOOFIT_DEBUG=ON` and `-DGOOFIT_TRACE=ON` will enable the matching printout macros

Advanced Options:
* `-DGOOFIT_HOST=Auto`: This is CPP unless device is `OMP`, in which case it is also `OMP`. This changes thrust::host_vector calculations, and is not fully supported when set to a non-default setting.
* `-DGOOFIT_TESTS=ON`: Build the GooFit tests
* `-DGOOFIT_SEPARATE_COMP=ON`: Enable separable compilation of PDFs. Single core CUDA builds are faster with this off.
* `-DGOOFIT_MPI=ON`: (OFF/ON.  With this feature on, GPU devices are selected automatically).  Tested with MVAPICH2/2.2 and OpenMPI.
* `-DGOOFIT_CUDA_OR_GROUPSIZE:INT=128`: This sets the group size that thrust will use for distributing the problem.  This parameter can be thought of as 'Threads per block'.  These will be used after running 'find_optimal.py' to figure out the optimal size.
* `-DGOOFIT_CUDA_OR_GRAINSIZE:INT=7`: This is the grain size thrust uses for distributing the problem.  This parameter can be thought of as 'Items per thread'.
* `-DGOOFIT_PYTHON=OFF`: Preliminary python bindings using [PyBind11].
* `-DGOOFIT_MAXPAR=1800`: The maximum number of parameters to allow. May cause memory issues if too large.
* You can enable sanitizers on non-CUDA builds with `-DSANITIZE_ADDRESS=ON`, `-DSANITIZE_MEMORY=ON`, `-DSANITIZE_THREAD=ON` or `-DSANITIZE_UNDEFINED=ON`.
* If `clang-tidy` is available, it will automatically be used to check the source. If you set `-DGOOFIT_TIDY_FIX=ON`, fixes will be applied to the GooFit source.

Note for targeting Tesla P100 or any `arch=6.0` device:
* Please use `-DGOOFIT_SEPARATE_COMP=ON` flags to compile.

A few standard CMake tricks:

* Use `make VERBOSE=1` to see the commands used to build the files.
* Use `cmake .. -LH` to list the CMake options with help.
* Use `ccmake` if available to see a curses (terminal) gui, or `cmake-gui` for a completely graphical interface.
* Use `-G` and the name of a generator to use something other than `make`, like `Xcode` or `Ninja`.
* Open the `CMakeLists.txt` with QtCreator to generate for that IDE.
* Set the release type with `-DCMAKE_BUILD_TYPE=Release`, `RelWithDebInfo`, `Debug`, etc.
* Set up multiple build directories, like `build-omp` and `build-cuda`.
* CMake caches your `-D` option selections in your build directory so you don't have to specify them again.
* CMake reruns when needed when you `make` unless you add a file that it globs for (like new `goofit_projects`).
* Use `make -j12` to build with 12 cores (for example). You can set this as the `MAKEFLAGS` environment variable, too.
* Use `CMake --build .` to build without referring to your specific build tool, like `make` or `ninja`.
* If you are using the `llvm` tool-suite, you can use `-DCMAKE_EXPORT_COMPILE_COMMANDS=ON` to generate the .json file that the `clang-*` commands expect.

> Note: Running `make`, `make omp`, or `make cuda` in the main directory will make a build directory for you, and will run CMake and make.

## Installing GooFit on different systems

The Docker command for each system is listed at the beginning of each command, to show setup from scratch. Ignore that line if you are not using Docker.

The following commands show you how to get a *minimal* install of GooFit on a vanilla system; you will probably want to add ROOT for your system, and possibly CUDA if you have a graphics card. If you do not have ROOT, some functionality, such as the Minuit1 version of the fitter, will not be available, and most of the examples will not be included in the build.

### CentOS 7

For simplicity, this uses EPEL to get access to `python-pip`, and uses the pip version of CMake. Feel free to download CMake directly from Kitware instead.

{% term %}
gitbook:~ $ docker run -it centos
root:/ # yum install epel-release
root:/ # yum install python-pip git gcc-c++ make -y
root:/ # pip install cmake plumbum
root:/ # git clone --recursive https://github.com/GooFit/GooFit.git
root:/ # cd GooFit
root:GooFit # mkdir build
root:GooFit # cd build
root:build # cmake ..
root:build # make
root:build # make test
{% endterm %}

If you'd like to add ROOT, add the following lines before running CMake:

{% term %}
root:build # mkdir root-6 && curl https://root.cern.ch/download/root_v6.08.06.Linux-centos7-x86_64-gcc4.8.tar.gz | tar --strip-components=1 -xz -C root-6
root:build # source root-6/bin/thisroot.sh
{% endterm %}

### Alpine Linux 3.5

A truly minimal system, Alpine gives you a working Docker system under 3 MB.

{% term %}
gitbook:~ $ docker run -it alpine
root:/ # apk add --no-cache make cmake g++ git libexecinfo-dev
root:/ # git clone --recursive https://github.com/GooFit/GooFit.git
root:/ # cd GooFit
root:GooFit # mkdir build
root:GooFit # cd build
root:build # cmake ..
root:build # make
root:build # ctest
{% endterm %}

In the spirit of minimality, this is less instructive and contains more magic, but also would also work:

{% term %}
gitbook:~ $ docker run -it alpine
root:/ # apk add --no-cache make cmake g++ git
root:/ # git clone https://github.com/GooFit/GooFit.git
root:GooFit # cd GooFit
root:GooFit # make
{% endterm %}

### Ubuntu 16.04

Ubiquitous Ubuntu works also. Ubuntu was used for the NVidia docker solution due to better support from NVidia. The following example uses ninja-build instead of make.

{% term style="ubuntu" %}
gitbook:~ $ docker run -it ubuntu
root:/ # apt-get update && apt-get install -y git cmake ninja-build g++
root:/ # git clone --recursive https://github.com/GooFit/GooFit.git
root:/ # cd GooFit
root:GooFit # mkdir build
root:GooFit # cd build
root:build # cmake ..
root:build # cmake --build .
root:build # ctest
{% endterm %}

If you'd like to add ROOT, add the following lines before running cmake:

{% term style="ubuntu" %}
root:build # mkdir root-6 && curl https://root.cern.ch/download/root_v6.08.06.Linux-ubuntu16-x86_64-gcc5.4.tar.gz | tar --strip-components=1 -xz -C root-6
root:build # source root-6/bin/thisroot.sh
{% endterm %}


### Note on CMake install

While other install paths, like `pip`, are easier, this way should always work. On Linux, you can manually get a version of CMake using:

{% term %}
gitbook:~ $ mkdir cmake && wget -qO- "https://cmake.org/files/v3.8/cmake-3.8.1-Linux-x86_64.tar.gz" | tar --strip-components=1 -xz -C cmake
gitbook:~ $ export PATH=`pwd`/cmake/bin:$PATH
{% endterm %}

The second line will need to be rerun whenever use a new shell. Feel free to make your updated CMake default; CMake is insanely backward compatible and will even "dumb itself down" when it sees a lower version in the `minimum_required` line in every `CMakeLists.txt`.
