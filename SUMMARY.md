# Summary

## Computing

* [Introduction](README.md)
* [Computing basics](computing/computing-basics.md)
* [GPU basics](gpu-basics.md)
* Baby steps in Python (X)
* [The Thrust library](computing/the-thrust-library.md)
* Device code (X)
* Mini GooFit (X)

## GooFit

* [GooFit installation](goofit/goofit-installation.md)
* [GooFit basics](goofit/goofit-basics.md)
* [GooFit analysis](goofit/goofit-analysis.md)
* GooFit Monte Carlo (X)
* [Adding a PDF](goofit/adding-a-pdf.md)
* [Modernizing GooFit](goofit/modernizing-classic-goofit-code.md)
* [Adding dependencies](goofit/adding-dependencies.md)
* PyGooFit (X)


