# The Thrust library

The Thrust library is possibly the easiest way to use CUDA from C++, and has the added benefit of supporting other CPU backends as well.